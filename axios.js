async function getName(url) {
    //OR axios.post
    axios.get(url, 
/*   Так можем передать параметры  {
        firstName: 'Fred',
        lastName: 'Flintstone'
    }
 */)
        .then(function (response) {
        var obj = response.data;
        var answer = document.getElementById('answer');
        
        for (key in obj) {
            var p = document.createElement("p");
            p.innerHTML = "<b>Name:</b> "+ obj[key].name.first + " " + obj[key].name.last;
            answer.appendChild(p);
        }
    })
        .catch(function (error) {
            var log = document.createElement("p");
            log.innerHTML = error;
            answer.appendChild(log);
    });
}
getName('https://unpkg.com/axios/dist/axios.min.js');